## ***__prerequisite__***  Docker and Docker compose
****Docker and Docker compose is required to run this project.****


## First Time Setup and Run

You�ll start by editing this README file to learn how to edit a file in Bitbucket.

1. ```git clone https://nisheetsun@bitbucket.org/nisheetsun/recruiterbox-assignment.git```
2. ```cd recruiterbox-assignment```
3. ```sh stop_all.sh``` ***WARNING : This stops all current running containers***
4. ```sh setup.sh```
5. ```sh run.sh```
6. ```sh migrate.sh``` **In new terminal***
	This will ask for name, email and password. Enter these credentials to be able to login to Django admin dashboard.
7. Go to http://localhost:8000/admin and login. Go to companys under [CUSTOMAUTH] heading and create new companies.
8. Go to  http://localhost:3000/ to access client web application.
---
## Run
1. ```sh run.sh```
2. Go to  http://localhost:3000/ to access client web application.
---

## Migration
***run migartion when docker-compose is up and running***
1. ```sh migrate.sh``` 


---
## About Repository
This is a repository which has two following *submodules*.

1. **Kudos** - [Django project](https://bitbucket.org/nisheetsun/kudos/src/master/)
2. **kudos-web** - [client web application](https://bitbucket.org/cwprivate/kudos-web/src/master/)

---

## Docker-compose services
1.  Server (kudos), a Django Server.
2. Web (kudos-web), a reactjs project.
3. Redis 
4. Postgres
5. Pgadmin

---

## kudos
https://bitbucket.org/nisheetsun/kudos/src/master/
kudos is a Django server. Djanogo uses postgres and redis databases.
*postgres* - is used to store user, employee, company and kudos data.
*redis* - is used to store kudos limit and availability.

Kudos has three following apps.

1. employee
2. kudos
3. customauth

---

## kudos-web
https://bitbucket.org/cwprivate/kudos-web/src/master/
This is a reactjs web application. 

---

## Links
1. http://localhost:8000 - Django project
2. http://localhost:3000 - client webapp
3. http://localhost:8000/admin  - Django project admin dashboard
4. http://localhost:5555/  -  PGadmin client

---

## Technology Used
**Database** - postgres, redis

**Frontend** - reactjs

**Backkend** - Django